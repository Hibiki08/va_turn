<?php

return [
    'adminEmail' => 'info@vaturn.ru',
    'projectEmail' => 'info@vaturn.ru',
    'host' => 'https://vaturn.ru',
    'phone' => '8 800 707 99 83',
    'count_standart' => 688,
    'count_comfort' => 100,
    'count_prem_platf' => 50,
    'count_prem_balk' => 16,
    'count_vip' => 12
];
