<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'name' => 'VA turn',
    'language' => 'rus-RUS',
    'sourceLanguage' => 'en',
    'timezone' => 'UTC',
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'PDQmS3mZxHE9LZDLhfxXM85NmApTu75O',
            'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'messageConfig' => [
                'charset' => 'UTF-8',
            ],
            'viewPath' => '@app/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.vaturn.ru',
                'username' => 'info@vaturn.ru',
                'password' => 'passBn987',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                '<action:(login|logout|reg|write-us|buy-ticket|pay-success|pay-fail|won|sent)>' => 'site/<action>',
                '<module:(admin)>' => '<module>/admin/index',
                '<module:(account)>' => '<module>/account/index',
                '<module:(admin)>/<action:[a-zA-Z0-9_\-]+>' => '<module>/admin/<action>',
                '<module:(account)>/<action:[a-zA-Z0-9_\-]+>' => '<module>/account/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>/<params:\w+>' => '<controller>/<action>',
//                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\AdminModule',
            'as access' => [ // if you need to set access
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['adminPermission'],
                    ],
                ]
            ],
        ],
        'account' => [
            'class' => 'app\modules\AccountModule',
            'as access' => [ // if you need to set access
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['accountPermission'],
                    ],
                ]
            ],
        ],
        'sberbank' => [
            'class' => 'pantera\yii2\pay\sberbank\Module',
            'components' => [
                'sberbank' => [
                    'class' => pantera\yii2\pay\sberbank\components\Sberbank::className(),

                    // время жизни инвойса в секундах (по умолчанию 20 минут - см. документацию Сбербанка)
                    // произвести оплату по выданной ему ссылке
                    'sessionTimeoutSecs' => 60 * 20,
                    'login' => 'vaturn-api',
                    'password' => 'alt98KdM>34dKl5',
                    // использовать тестовый режим (по умолчанию - нет)
                    'testServer' => false,
                ],
            ],

            // страница вашего сайта с информацией об успешной оплате
            'successUrl' => '/?pay=success',

            // страница вашего сайта с информацией о НЕуспешной оплате
            'failUrl' => '/pay-fail',

            // обработчик, вызываемый по факту успешной оплаты
            'successCallback' => function($invoice) {
                if ($invoice->status == 'S') {
                    $order = app\models\Order::findOne($invoice->order_id);
                    if (!empty($order)) {
                        $client = app\models\Customer::findOne($order->customer_id);
                        if (!empty($client)) {
                            if (isset($invoice->data['type'])) {
                                $ticketAmount = app\models\Ticket::countByType($invoice->data['type']);
                                $ticket_type = app\models\TicketType::findOne($invoice->data['type']);
//                                $ticket = app\models\Ticket::getOneByType($invoice->data['type']);
                                if ($ticketAmount < $ticket_type->amount) {
                                    $ticket = new \app\models\Ticket($invoice->data['type']);
                                    $newTicket = $ticket->createTicket();
                                    if (!empty($newTicket)) {
                                        $order->ticket_id = $newTicket->id;
                                        $order->status_pay = 1;
                                        $order->update();

                                        if (isset($invoice->data['link'])) {
                                            $code = $invoice->data['link'];
                                            $partner = app\models\User::findByLink($code);
                                            if (!empty($partner)) {
                                                $us_cust = app\models\UserCustomer::findOne(['user_id' => $partner->id, 'customer_id' => $client->id]);
                                                if (empty($us_cust)) {
                                                    $add_client = new app\models\UserCustomer();
                                                    $add_client->user_id = $partner->id;
                                                    $add_client->customer_id = $client->id;
                                                    $add_client->save();
                                                }
                                            }
                                        }

                                        $create = $newTicket->saveTicket($invoice->data['addition_number'], $invoice->data['sum']);
                                        if ($create) {
                                            $attachment = dirname(__DIR__) . '/web/doc/tickets/' . $newTicket->number . '.jpg';
                                            if (file_exists($attachment)) {
                                                $subject = 'Оплата на сайте ' . Yii::$app->name;
                                                $text = '<p>Произведена покупка билета №' . $newTicket->number . ' на сайте ' . Yii::$app->params['host'] . '</p>' .
                                                '<p>Подробную информацию о фестивале и билетах вы можете посмотреть в прикреалённом к письму файле.</p>';

                                                $sent = $client->sendEmail($subject, $text, $attachment);

                                                $newTicket->status = 1;
                                                $newTicket->update();

                                                if ($sent) {
                                                    Yii::$app->mailer->compose()
                                                        ->setFrom([Yii::$app->params['projectEmail'] => Yii::$app->name . ' robot'])
                                                        ->setTo(Yii::$app->params['adminEmail'])
                                                        ->setSubject('Покупка билета на сайте ' . Yii::$app->name)
                                                        ->setHtmlBody('<p>Покупка на сайте билета №' . $newTicket->number . '</p><p>email: ' . $client->email . '</p>')
                                                        ->send();
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $subject = 'Оплата на сайте ' . Yii::$app->name;
                                    $text = 'При покупке билета возникла ошибка. Пожалуйста, связижесть с администратором по телефону: ' . Yii::$app->params['phone'] . ' или через email: ' . Yii::$app->params['projectEmail'];

                                    $client->sendEmail($subject, $text);
                                }
                            }
                        }
                    }
                }
            }
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
