<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Cormorant:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext',
        'https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext',
        'fonts/material-design-iconic-font/css/materialdesignicons.min.css',
        'lib/slick-1.8.0/slick.css',
        'lib/jquery-ui-1.12.1.custom/jquery-ui.min.css',
        'css/style.css?r=0.25',
    ];
    public $js = [
        'js/jquery-2.1.1.min.js',
        'https://3dsec.sberbank.ru/demopayment/docsite/assets/js/ipay.js',
        'lib/slick-1.8.0/slick.min.js',
        'js/map.js?r=0.1',
        'lib/jquery-ui-1.12.1.custom/jquery-ui.js',
        'js/maskedinput.js',
        'js/script.js?r=0.6',
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
