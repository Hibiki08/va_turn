$(document).ready(function () {
    $('.phone-mask').mask('+7 (999) 999-99-99');
    $('.birthday').mask('99-99-9999');

    $('.slider-members').slick({
        dots: true,
        dotsClass: 'custom_paging',
        customPaging: function (slider, i) {
            return '<span class="big">' + (i + 1) + '</span><span class="small">' + '/' + slider.slideCount + '</span>';
        },
        appendArrows: $('.members .arrows'),
        prevArrow: '<div class="prev"><i class="mdi mdi-chevron-left"></i></div>',
        nextArrow: '<div class="next"><i class="mdi mdi-chevron-right"></i></div>'
    });

    $('.slider-products').slick({
        appendArrows: $('.fair-block .arrows'),
        prevArrow: '<div class="prev"><i class="mdi mdi-chevron-left"></i></div>',
        nextArrow: '<div class="next"><i class="mdi mdi-chevron-right"></i></div>'
    });

    $('.slider-tickets').slick({
        dots: true,
        dotsClass: 'custom_paging',
        customPaging: function (slider, i) {
            return '<span class="big">' + (i + 1) + '</span><span class="small">' + '/' + slider.slideCount + '</span>';
        },
        appendDots: $('.move .dots'),
        appendArrows: $('.move .arrows'),
        prevArrow: '<div class="prev"><i class="mdi mdi-chevron-left"></i></div>',
        nextArrow: '<div class="next"><i class="mdi mdi-chevron-right"></i></div>'
    });

    $('header .info nav').click(function () {
        $('.menu-open').fadeIn(300);
    });
    $('.menu-open .cross').click(function () {
        $('.menu-open').fadeOut(300);
    });
    $('.popup.enter .sub div').click(function () {
        $('.popup.enter .sub p').slideToggle();
    });
    $('#tickets .active').click(function () {
        $('#tickets .tickets-list').slideToggle(200);
    });
    $('.popup.buy-ticket').click(function (e) {
        var elem = e.target;
        if ($(elem).hasClass('buy-ticket') || $(elem).hasClass('width')) {
            $(this).fadeOut(300);
        }
    });
    $('.popup.enter').click(function (e) {
        var elem = e.target;
        if ($(elem).hasClass('enter') || $(elem).hasClass('width')) {
            $(this).fadeOut(300);
        }
    });
    $('#tickets .tickets-list li:not(".disabled")').click(function () {
        var id = $(this).data('id');
        var price = $(this).data('price');
        var name = $(this).html();

        $('.buy-ticket .choose .text p').removeClass('active');
        $(this).parents('.choose').find('.text p[data-id=' + id + ']').addClass('active');

        $('.choose #buyticketform-ticket_id').val(id);
        $('#tickets .active').html(name + '<span class="price-ticket">' + price + ' <span>р.</span></span>');
        calculator();
        $('#tickets .tickets-list').slideUp(200);

    });
    $('.popup.pay-success').click(function (e) {
        var elem = e.target;
        if ($(elem).hasClass('pay-success') || $(elem).hasClass('width')) {
            window.location = '/';
        }
    });
    $('#buy-more').click(function () {
        $('.popup.pay-success').fadeOut(300);
        $('.buy').click();
    });
    $('.buy').click(function () {
        $('.popup.buy-ticket').fadeIn(300);
    });
    $('.form-cont input[type=text]').focusin(function () {
        $(this).parents('.block-input').addClass('focus');
    }).focusout(function () {
        $(this).parents('.block-input').removeClass('focus');
    });
    
    $('#remind .button').click(function () {
        $('#remind .block-input').removeClass('has-error');
        $('#remind i').data('error', '');
        var val = $('#remind').find('input').val();
        
        $.ajax({
            url: '/site/remind',
            type: 'post',
            data: {
                email: val
            },
            timeout: 600000,
            success: function (data) {
                console.log('ok');
                console.log(data);
                if (data.status > 0) {
                    $('.popup.message').fadeIn(300);
                    $('#remind input').val('');
                    // yaCounter40107886.reachGoal('broadcast');
                }
                if (data.error.email.length > 0) {
                    $('#remind .block-input').addClass('has-error');
                    $('#remind i').data('error', data.error.email);
                }
            },
            error: function (data) {
                console.log('bad');
                console.log(data);
            }
        });
    });

    $('#support .button').click(function () {
        $('#support .block-input').removeClass('has-error');
        $('#support i').data('error', '');
        var email = $('#support').find('input').val();
        var text = $('#support').find('textarea').val();

        $.ajax({
            url: '/write-us',
            type: 'post',
            data: {
                email: email,
                text: text
            },
            timeout: 600000,
            success: function (data) {
                console.log('ok');
                console.log(data);
                if (data.status > 0) {
                    $('footer .text-success').fadeIn(300);
                    $('#support input').val('');
                    $('#support textarea').val('');
                    // yaCounter40107886.reachGoal('question');
                }
                if (data.error.email.length > 0) {
                    $('#support input').parents('.block-input').addClass('has-error');
                    $('#support input').parents('.block-input').find('i').data('error', data.error.email);
                }
                if (data.error.text.length > 0) {
                    $('#support textarea').parents('.block-input').addClass('has-error');
                    $('#support textarea').parents('.block-input').find('i').data('error', data.error.text);
                }
            },
            error: function (data) {
                console.log('bad');
                console.log(data);
            }
        });
    });

    $('.popup.message .btn').click(function () {
        $('.popup.message').fadeOut(300);
    });

    $('.mdi-alert-circle').mouseover(function () {
        var pos = $(this).data('pos');
        var top = $(this).offset().top - 15;
        var left = $(this).offset().left;
        var text = $(this).data('error');

        $('#error-block .text').html(text);
        $('#error-block .tail').addClass(pos);

        var blockWidth = $('#error-block').outerWidth();

        if (pos == 'right') {
           left = left - blockWidth - 40;
        }
        $('#error-block').css({
            left: left + 'px',
            top: (top - $('#error-block').height()) + 'px',
            display: 'block'
        });
    }).mouseout(function () {
        var pos = $(this).data('pos');
        $('#error-block .text').html('');
        $('#error-block .tail').removeClass(pos);
        $('#error-block').attr('style', '').hide();
    });

    //сбер
    $('#pay').click(function () {
        // yaCounter40107886.reachGoal('buy_ticket');
        $(this).parents('form').submit();
    });

    $('.popup.pay-success .anchor').click(function () {
        $('.popup.pay-success').fadeOut(300);
    });

    $('a.anchor').click(function (e) {
        e.preventDefault();
        var elem = $(this).attr('href');
        var top = $(elem).offset().top;
        $('html, body').animate({
            scrollTop: top
        }, 800);
    });
    
    $('.menu-open .list a').click(function () {
        $('.menu-open').fadeOut(300);
    });

    $('#additionally .mdi-menu-right').click(function () {
        var value = parseInt($(this).parents('.arrow-common').find('input').val());
        var price = parseInt($(this).parents('.arrow-common').find('.summ span').data('price'));
        if (value < 100) {
            value += 1;
            var add = value * price;
            $(this).parents('.arrow-common').find('input').val(value);
            $(this).parents('.arrow-common').find('.summ span').text(add);
            calculator();
        }
    });
    $('#additionally .mdi-menu-left').click(function () {
        var value = parseInt($(this).parents('.arrow-common').find('input').val());
        var price = parseInt($(this).parents('.arrow-common').find('.summ span').data('price'));
        if (value > 0) {
            value -= 1;
            var add = value * price;
            $(this).parents('.arrow-common').find('input').val(value);
            $(this).parents('.arrow-common').find('.summ span').text(add);
            calculator();
        }
    });

    $('.common.checkbox-common').click(function () {
        calculator();
    });

    function calculator() {
        var ticketPrice = parseInt($('#tickets .active .rub').text());
        var arrowCommonSum = 0;
        var checkboxCommonSum = 0;

        $('#additionally .common.arrow-common').each(function () {
            var arrow_value = 0;
            var arrow_price = 0;
            var add = 0;
            arrow_value = parseInt($(this).find('input').val());
            arrow_price = parseInt($(this).find('.summ span').data('price'));
            add = arrow_value * arrow_price;
            arrowCommonSum += add;
        });

        $('#additionally .common.checkbox-common').each(function () {
            var checkbox_price = 0;
            if ($(this).find('input[type="checkbox"]').prop('checked')) {
                checkbox_price = parseInt($(this).find('input[type="checkbox"]').parents('label').find('.summ span').data('price'));
            }

            checkboxCommonSum = checkboxCommonSum + checkbox_price;
        });

        var sum = ticketPrice + arrowCommonSum + checkboxCommonSum;
        $('.description .choose .price .int').text(sum)
    }
});