function deleteAjax($this, url, model) {
    $this.find('.progress').show();

    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            id: $this.data().id,
            model: model
        },
        success: function (response) {
            if (response.status == true) {
                $this.closest('tr').remove();
            }
        },
        error: function () {
            $this.find('.progress').hide();
        }
    });
}

$(document).ready(function () {
    $('.stat-content .came').click(function () {
        var id = $(this).data('id');
        var value = $(this).prop("checked") ? 1 : 0;
        $('body').fadeTo(100, 0.3);

        $.ajax({
            url: '/admin/came',
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
                value: value,
                _csrf: yii.getCsrfToken()
            },
            success: function (response) {
                if (response.status == true) {
                    $('body').fadeTo(100, 1);
                }
            },
            error: function () {
                $('body').fadeTo(100, 1);
            }
        });
    });
});