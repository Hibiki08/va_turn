<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

/**
 * This is the model class for table "ticket_types".
 *
 * @property integer $id
 * @property integer $number
 * @property string $name
 * @property string $price
 * @property integer $amount
 * @property string $description
 */
class TicketType extends ActiveRecord {

    const COMFORT_AMOUNT = 920;
    const PREMIUM_AMOUNT = 24;
    const VIP_AMOUNT = 12;
    const PRESIDENT_AMOUNT = 1;

    public static function tableName() {
        return 'ticket_types';
    }

    public static function getAll() {
        return self::find()->orderBy('id')
            ->where(['archive' => 0])
            ->all();
    }

    public function getTickets() {
        return $this->hasMany(Ticket::className(), ['type_id' => 'id'])
            ->alias('ticket');
    }
    
    public static function findById($id) {
        return self::findOne($id);
    }

    public function getLeft() {
        $count = $this->hasMany(Ticket::className(), ['type_id' => 'id'])
            ->where(['archive' => 0])
            ->andWhere(['status' => 1])
        ->count();
        
        switch ($this->id) {
            case Ticket::TYPE_COMFORT:
                return (int)($count < self::COMFORT_AMOUNT);
                break;
            case Ticket::TYPE_VIP:
                return (int)($count < self::VIP_AMOUNT);
                break;
            case Ticket::TYPE_PREMIUM:
                return (int)($count < self::PREMIUM_AMOUNT);
                break;
            case Ticket::TYPE_PRESIDENT:
                return (int)($count < self::PRESIDENT_AMOUNT);
                break;
        }
        return 0;
    }

}