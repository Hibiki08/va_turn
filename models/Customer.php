<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

/**
 * This is the model class for table "customers".
 *
 * @property integer $id
 * @property string $email
 * @property string $phone
 * @property string $name
 * @property string $birth_date
 */
class Customer extends ActiveRecord {

    public static function tableName() {
        return 'customers';
    }

    public static function getAll() {
        return self::find()->orderBy('id')->all();
    }

    public static function findByEmail($email) {
        return self::findOne(['email' => $email]);
    }
    
    public function sendEmail($subject, $text, $attach = false) {
        $mail = Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['projectEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject($subject)
            ->setHtmlBody($text);

        if ($attach) {
            $mail->attach($attach);
            $mail->attach(dirname(__DIR__) . '/web/doc/Instruktsia_VA_Turn_2.pdf');
        }
        return $mail->send();
    }

    public function getOrders() {
        return $this->hasMany(Order::class, ['customer_id' => 'id'])
            ->where(['status_pay' => 1]);
    }

}