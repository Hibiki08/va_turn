<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

/**
 * This is the model class for table "newsletter".
 *
 * @property integer $id
 * @property string $email
 */
class Newsletter extends ActiveRecord {
    
    public static function tableName() {
        return 'newsletter';
    }

    public static function getAll() {
        return self::find()->orderBy('id')->all();
    }

    public function rules() {
        return [
            [['email'], 'email', 'message' => '<strong>Email введен неверно!</strong> Введите еще раз в формате <span>example@mail.ru</span>'],
            [['email'], 'unique', 'targetClass' => self::className(), 'message' => 'Вы уже подписаны на рассылку'],
            [['email'], 'required'],
        ];
    }
    
    public static function findByEmail($email) {
        return self::findOne(['email' => $email]);
    }

}