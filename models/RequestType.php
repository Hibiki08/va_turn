<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

/**
 * This is the model class for table "requests_type".
 *
 * @property integer $id
 * @property string $name
 */
class RequestType extends ActiveRecord {
    
    const WRITE_US = 1;

    public static function tableName() {
        return 'requests_type';
    }
    
}