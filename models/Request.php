<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "requests".
 *
 * @property integer $id
 * @property string $email
 * @property string $text
 * @property integer $type_id
 * @property integer $status
 * @property string $created_at
 */
class Request extends ActiveRecord {
    
    const SCENARIO_REQUEST = 'request';

    public static function tableName() {
        return 'requests';
    }

    public function rules() {
        return [
            [['email'], 'email', 'message' => '<strong>Email введен неверно!</strong> Введите еще раз в формате <span>example@mail.ru</span>', 'on' => self::SCENARIO_REQUEST],
            [['email', 'text'], 'required', 'on' => self::SCENARIO_REQUEST],
            [['created_at'], 'safe'],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_REQUEST] = ['email', 'text'];
        return $scenarios;
    }

    public function getTypeName() {
        return $this->hasOne(RequestType::tableName(), ['id' => 'type_id']);
    }

    public static function getAllByTypeId($type_id) {
        return self::find()->where(['type_id' => $type_id])->orderBy('created_at')->all();
    }

    public function search($params) {
        $query = self::find()
            ->andWhere(['archive' => 0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [20, 50, 100, 300],
            ],
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->created_at)) {
            $query->andFilterWhere(['like', 'created_at', Yii::$app->formatter->asDate($this->created_at, 'yyyy-MM-dd')]);
        }

        return $dataProvider;
    }

}