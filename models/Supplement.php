<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "supplement".
 *
 * @property integer $id
 * @property string $title
 * @property integer $price
 * @property string $key_word
 * @property string $description
 * @property integer $sort
 */
class Supplement extends ActiveRecord {
    
    const PHOTO_ID = 1;
    const EARLY_ID = 2;
    const BOX_ID = 3;
    const BEHIND_ID = 4;
    const TOO_EARLY_ID = 5;

    public static function tableName() {
        return 'supplement';
    }
    
    public static function getAllSupplement() {
        return self::find()
            ->orderBy('sort')
            ->all();
    }
    
    public static function findById($id) {
        return self::findOne($id);
    }
    
//    public static function getPhotoPrice() {
//        return self::findOne(self::PHOTO_ID)
//            ->price;
//    }
    
}