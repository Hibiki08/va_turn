<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tickets".
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $number
 * @property integer $status
 * @property integer $came
 */
class Ticket extends ActiveRecord {

    public $ticketNumber;
    public $ticketEmail;
    public $ticketName;
    public $ticketOrder;
    private $ticketType;

    const TYPE_STANDART = 1;
    const TYPE_COMFORT = 2;
    const TYPE_PREM_PLAT = 3;
    const TYPE_PREM_BALK = 4;
    const TYPE_VIP = 5;
    const TYPE_PREMIUM = 6;
    const TYPE_PRESIDENT = 7;

    public static function tableName() {
        return 'tickets';
    }

    public function __construct($ticketType = null) {
        parent::__construct();
        $this->ticketType = $ticketType;
    }

    public function rules() {
        return [
            [['ticketNumber', 'ticketOrder'], 'integer'],
            [['ticketEmail', 'ticketName'], 'string'],
//            [['date','family' ,'date_close'], 'safe'],
        ];
    }

    public static function getAll() {
        return self::find()->orderBy('id')->all();
    }
    
    public static function getBought() {
        return self::find()
            ->where(['status' => 1])
            ->andWhere(['archive' => 0])
            ->orderBy('id')->all();
    }
    
    public function getType() {
        return $this->hasOne(TicketType::class, ['id' => 'type_id']);
    }

    public function getOrder() {
        return $this->hasOne(Order::class, ['ticket_id' => 'id'])
            ->joinWith('client', true)
            ->alias('order');
    }

    public static function getOneByType($type) {
        return self::findOne(['type_id' => $type, 'status' => 0]);
    }

    public static function countByType($type) {
        return self::find()
        ->where(['type_id' => $type, 'status' => 1, 'archive' => 0])
            ->count();
    }

    public static function countAll() {
        return self::find()
            ->where(['archive' => 0])
            ->count();
    }

    public function createTicket() {
        if (!empty($this->ticketType)) {
            $type = TicketType::findById($this->ticketType);
            if (!empty($type)) {
                $typeAmount = self::countByType($type);
                $typeAmount = str_replace('.', '', (string)(($typeAmount + 1) / 100));
                $allAmount = self::countAll();
                $allAmount = str_replace('.', '', (string)(($allAmount + 1) / 100));

                $number = (int)($type->number . $typeAmount . $allAmount . rand(100, 999));
                $ticket = new self;
                $ticket->type_id = $type->id;
                $ticket->number = $number;
                if ($ticket->save()) {
                    return $ticket;
                }
            }
        }
        return null;
    }

    public function saveTicket($additional = '', $sum = 0) {
        $number = (string)$this->number;
        $number = $number[0] . $number[1] . ' ' . $number[2] . $number[3] . $number[4] . ' ' . $number[5] . $number[6] . $number[7] . ' ' . $number[8] . $number[9] . $number[10];
        $numberString = (!empty($additional) ? $additional . ' ' : '') . $number;

        $template = '1.jpg';
        switch($this->type_id) {
            case self::TYPE_COMFORT:
                $template = 'comfort-3.jpg';
                break;
            case self::TYPE_PREMIUM:
                $template = 'premium-3.jpg';
                break;
            case self::TYPE_VIP:
                $template = 'vip-3.jpg';
                break;
            case self::TYPE_PRESIDENT:
                $template = 'president-3.jpg';
                break;
        }
        $path = dirname(__DIR__) . '/web/images/ticket/' . $template;
        if (file_exists($path)) {
            $image = imagecreatefromjpeg($path);
            $color = imagecolorallocate($image, 255, 244, 0);
            $color2 = imagecolorallocate($image, 33, 33, 33);

            $montserrat = dirname(__DIR__) . '/web/fonts/Montserrat/Montserrat-Black.ttf';
            $ntf = dirname(__DIR__) . '/web/fonts/NTF-Grand/NTF-Grand-Regular.ttf';

            imagettftext($image, 41, 90, 1905, 770, $color, $montserrat, $numberString);
            imagettftext($image, 59, 90, 2167, 400, $color2, $ntf, $number);
            imagettftext($image, 115, 90, 2225, 770, $color2, $ntf, $sum . 'p.');

            $res = imagejpeg($image,  dirname(__DIR__) . '/web/doc/tickets/' . $this->number . '.jpg', 95);
            imagedestroy($image);
            return $res;
        }
        return false;
    }

    public function search($params) {
        $this->load($params);
        
        $query = self::find()->where(['status' => 1])
            ->andWhere([self::tableName() . '.archive' => 0])
            ->joinWith('order', true)
            ->orderBy('id');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', self::tableName() . '.number', $this->ticketNumber]);
        $query->andFilterWhere(['like', 'client.email', $this->ticketEmail]);
        $query->andFilterWhere(['like', 'client.name', $this->ticketName]);
        $query->andFilterWhere(['like', 'order.id', $this->ticketOrder]);

        return $dataProvider;
    }
    
}