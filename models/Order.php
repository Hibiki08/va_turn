<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $ticket_id
 * @property integer $status_pay
 * @property string $pay_data
 */
class Order extends ActiveRecord {


    public static function tableName() {
        return 'orders';
    }

    public static function getAll() {
        return self::find()->orderBy('id')->all();
    }

    public function getClient() {
        return $this->hasOne(Customer::class, ['id' => 'customer_id'])
            ->alias('client');
    }

    public function getTicket() {
        return $this->hasOne(Ticket::class, ['id' => 'ticket_id']);
    }

    public static function findAllByUsers($users) {
        return self::find()
            ->where(['in', 'customer_id', $users])
            ->andWhere(['status_pay' => 1])
            ->all();
    }
    
}