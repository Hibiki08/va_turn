<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;


class BuyTicketForm extends Model {

    public $name;
    public $phone;
    public $date;
    public $email;
    public $subscribe = 1;
    public $ticket_id;
    public $photo = 0;
    public $early = 0;
    public $too_early = 0;
    public $box = 0;
    public $behind = 0;

    public function rules() {
        return [
            [['email'], 'email'],
            [['email', 'phone', 'name', 'ticket_id'], 'required'],
            ['phone', 'match', 'pattern' => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/'],
            ['date', 'match', 'pattern' => '/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/'],
            [['photo', 'early', 'box', 'behind', 'too_early'], 'supplementValidation'],
            [['name', 'phone', 'email', 'date'], 'filter', 'filter' => 'trim'],
        ];
    }

    public function supplementValidation($attribute, $params) {
        if ($this->$attribute < 0) {
            $this->addError($attribute, 'Число должно быть положительным!');
            return false;
        }
        return true;
    }

    public function attributeLabels() {
        return [
            'phone' => 'Телефон',
            'name' => 'Имя',
            'email' => 'Email',
            'date' => 'Дата рождения'
        ];
    }

}