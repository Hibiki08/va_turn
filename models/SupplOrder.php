<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "suppl_order".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $suppl_id
 * @property integer $amount
 */
class SupplOrder extends ActiveRecord {

    public static function tableName() {
        return 'suppl_order';
    }

}