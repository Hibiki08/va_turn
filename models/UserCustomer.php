<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

/**
 * This is the model class for table "user_customers".
 *
 * @property integer $user_id
 * @property integer $customer_id
 */
class UserCustomer extends ActiveRecord {

    public static function tableName() {
        return 'user_customers';
    }

    public static function getAll() {
        return self::find()->orderBy('id')->all();
    }
    
    public function getCustomer() {
        return $this->hasOne(Customer::class, ['id' => 'customer_id']);
    }
    
//    public function getUser() {
//        return $this->hasOne(User::class, ['id' => 'user_id']);
//    }

    public static function findAllByPartner() {
        return self::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->all();
    }

}