var gulp = require('gulp'),
    less = require('gulp-less'),
    mmq = require('gulp-merge-media-queries'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    csso = require('gulp-csso'),
    smartgrid = require('smart-grid'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css');

gulp.task('watch', function () {
    gulp.watch('web/less/*.less', ['less']);
});

gulp.task('cssmin', function () {
    return gulp.src('web/css/style.css')
        .pipe(cleanCSS({compatibility: 'ie8'}).on('error', function(err) {
            console.log(err);
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('web/css'));
});

gulp.task('less', function () {
    return gulp.src('web/less/*.less')
        .pipe(less().on('error', function (err) {
            console.log(err);
        }))
        .pipe(csso()) //объединение классов
        .pipe(mmq({ //объединение media
            log: true
        }))
        .pipe(autoprefixer({ //автопроставление префиксов
            browsers: ['last 2 version', 'safari 6', 'safari 5.1', 'ie 6', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 7', 'android 4', 'firefox 2'],
            cascade: false
        }))
        .pipe(gulp.dest('web/css'));
});

gulp.task('autoprefixer', function () {
    return gulp.src('web/css/style.css')
        .pipe(autoprefixer({
            browsers: ['last 2 version', 'safari 6', 'safari 5.1', 'ie 6', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 7', 'android 4', 'firefox 2'],
            cascade: false
        }))
        .pipe(gulp.dest('web/css'));
});

/* It's principal settings in smart grid project */
var settings = {
    outputStyle: 'less', /* less || scss || sass || styl */
    columns: 24, /* number of grid columns */
    offset: "20px", /* gutter width px || % */
    container: {
        maxWidth: '1210px', /* max-width оn very large screen */
        fields: '30px' /* side fields */
    },
    breakPoints: {
        bg: {
            'width': '1205px',
            'fields': '20px'
        },
        lg: {
            'width': '1050px', /* -> @media (max-width: 1100px) */
            'fields': '20px', /* side fields */
        },
        md: {
            'width': '960px',
            'fields': '20px'
        },
        sm: {
            'width': '780px',
            'fields': '20px'
        },
        ls: {
            'width': '907px',
            'fields': '20px'
        },
        xs: {
            'width': '560px',
            'fields': '20px'
        },
        vs: {
            'width': '500px',
            'fields': '20px'
        }
        /*
         We can create any quantity of break points.

         some_name: {
         some_width: 'Npx',
         some_offset: 'N(px|%)'
         }
         */
    }
};

smartgrid('web/less', settings);

gulp.task('default', ['less', 'watch']);
