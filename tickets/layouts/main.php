<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <style>
        body {
            display: inline-block;
        }
        .content {
            width: 100%;
            /*width: 827px;*/
            position: relative;
        }
        .content .img {
            width: 100%;
        }
        .content .img img {
            width: 100%;
        }
        .number {
            font-family: 'Montserrat', sans-serif;
            font-size: 60px;
            font-weight: 800;
            position: absolute;
            color: #fff400;
            right: 67px;
            bottom: 427px;
        }
        .barcode {
            position: absolute;
            right: 73px;
            bottom: 78px;
        }
        .barcode img {
            width: 100%;
        }
    </style>
</head>
<body>
<div class="content">
    <?php echo $content; ?>
</div>
</body>
</html>