<?php

namespace app\controllers;

use app\components\enums\ReportType;
use app\models\Customer;
use app\models\Order;
use app\models\Supplement;
use app\models\SupplOrder;
use app\models\Ticket;
use app\models\TicketType;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use yii\helpers\Url;
use app\models\Newsletter;
use app\models\Request;
use app\models\RequestType;
use yii\web\Cookie;
use app\models\forms\BuyTicketForm;
use pantera\yii2\pay\sberbank\models\Invoice;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action) {
        if ($action->id == 'remind' || $action->id == 'write-us' || $action->id == 'pay-success' || $action->id == 'pay-fail') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $this->layout = 'index';

//        $ticket = new \app\models\Ticket(7);
////        $ticket_id = $ticket->createTicket();
//        $ticket = $ticket->createTicket();
//        $create = $ticket->saveTicket('01 03 101', 1526);
//        var_dump($create);
//        if ($create) {
//            $client = \app\models\Customer::findOne(6);
//            $attachment = dirname(__DIR__) . '/web/doc/tickets/' . $ticket->number . '.jpg';
//            var_dump($attachment);
//            var_dump(file_exists($attachment));
//            if (file_exists($attachment)) {
//                $subject = 'Оплата на сайте ' . Yii::$app->name;
//                $text = 'test';
//
//                $ticket->status = 1;
//                $ticket->update();
//
//                $sent = $client->sendEmail($subject, $text, $attachment);
//                var_dump($sent);
//
//            }
//        }
//        die;
        
        $showAdditions = false;
        $form = new BuyTicketForm();
        $code = null;
        $first = null;
        $customer_email = null;
        $session = Yii::$app->session;


        $get = Yii::$app->request->get();
        if (isset($get['link'])) {
            $session->set('lnk', $get['link']);
        }

        if ($session->has('lnk')) {
            $code = $session->get('lnk');
        }
        
        if ($session->has('customer_email')) {
            $customer_email = $session->get('customer_email');
        }

        $tickets = TicketType::getAll();
        $first = $tickets[0];
        
        $supplement = Supplement::getAllSupplement();

//        foreach ($tickets as $ticket) {
//            if ($ticket->count != 0) {
//                $first = $ticket;
//                break;
//            }
//        }
        
            if ($form->load(Yii::$app->request->post()) && $form->validate()) {
                $ticket_type = TicketType::findOne($form->ticket_id);
                $ticketAmount = Ticket::countByType($form->ticket_id);
                if (!empty($ticket_type)) {
                    if ($form->subscribe) {
                        $newsletter = Newsletter::findByEmail($form->email);
                        if (empty($newsletter)) {
                            $newsletter = new Newsletter();
                            $newsletter->email = $form->email;
                            $newsletter->save();
                        }
                    }
                    if ($ticketAmount < $ticket_type->amount) {
                        $customer = Customer::findByEmail($form->email);
                        if (empty($customer)) {
                            $customer = new Customer();
                            $customer->email = $form->email;
                            $customer->phone = $form->phone;
                            $customer->name = $form->name;
                            $customer->birth_date = date('Y-m-d', strtotime($form->date));
                            $customer->save();
                            $session->set('customer_email', $customer->email);
                        }
                        $order = new Order();
                        $order->customer_id = $customer->id;
                        if ($order->save()) {
                            $photo = 0;
                            $early = 0;
                            $too_early = 0;
                            $box = 0;
                            $behind = 0;
                            $addition_number = '';
                            if ($form->photo > 0) {
                                $supplOrder = new SupplOrder();
                                $supplOrder->order_id = $order->id;
                                $supplOrder->suppl_id = Supplement::PHOTO_ID;
                                $supplOrder->amount = $form->photo;
                                $supplOrder->save();

                                $item = Supplement::findOne($supplOrder->suppl_id);
                                if (!empty($item)) {
                                    $photo = (int)$item->price * (int)$form->photo;
                                }
                                $addition_number = ($form->photo > 10 ? $form->photo : '0' . $form->photo) . ' ';
                            } else {
                                $addition_number = '00 ';
                            }
                            if ($form->box > 0) {
                                $supplOrder = new SupplOrder();
                                $supplOrder->order_id = $order->id;
                                $supplOrder->suppl_id = Supplement::BOX_ID;
                                $supplOrder->amount = $form->box;
                                $supplOrder->save();

                                $item = Supplement::findOne($supplOrder->suppl_id);
                                if (!empty($item)) {
                                    $box = (int)$item->price * (int)$form->box;
                                }
                                $addition_number .= ($form->box > 10 ? $form->box : '0' . $form->box) . ' ';
                            } else {
                                $addition_number .= '00 ';
                            }
                            if ($form->early > 0) {
                                $supplOrder = new SupplOrder();
                                $supplOrder->order_id = $order->id;
                                $supplOrder->suppl_id = Supplement::EARLY_ID;
                                $supplOrder->amount = $form->early;
                                $supplOrder->save();

                                $item = Supplement::findOne($supplOrder->suppl_id);
                                if (!empty($item)) {
                                    $early = (int)$item->price * (int)$form->early;
                                }
                                $addition_number .= '1';
                            } else {
                                $addition_number .= '0';
                            }
                            if ($form->too_early > 0) {
                                $supplOrder = new SupplOrder();
                                $supplOrder->order_id = $order->id;
                                $supplOrder->suppl_id = Supplement::TOO_EARLY_ID;
                                $supplOrder->amount = $form->early;
                                $supplOrder->save();

                                $item = Supplement::findOne($supplOrder->suppl_id);
                                if (!empty($item)) {
                                    $too_early = (int)$item->price * (int)$form->too_early;
                                }
                                $addition_number .= '1';
                            } else {
                                $addition_number .= '0';
                            }
                            if ($form->behind > 0) {
                                $supplOrder = new SupplOrder();
                                $supplOrder->order_id = $order->id;
                                $supplOrder->suppl_id = Supplement::BEHIND_ID;
                                $supplOrder->amount = $form->behind;
                                $supplOrder->save();

                                $item = Supplement::findOne($supplOrder->suppl_id);
                                if (!empty($item)) {
                                    $behind = (int)$item->price * (int)$form->behind;
                                }
                                $addition_number .= '1';
                            } else {
                                $addition_number .= '0';
                            }
                            
                            $sum = ($ticket_type->price + $photo + $early + $too_early + $box + $behind);

                            $invoice = Invoice::addSberbank($order->id, $sum, null, ['link' => $code, 'type' => $form->ticket_id, 'addition_number' => $addition_number, 'sum' => $sum]);
                            return Yii::$app->getResponse()->redirect(Url::toRoute(['/sberbank/default/create', 'id' => $invoice->id]));
                        }
                    }
                }
            }
        
        return $this->render('index', [
            'tickets' => $tickets,
            'ticket_form' => $form,
            'first' => $first,
            'customer_email' => $customer_email,
            'supplement' => $supplement,
            'showAdditions' => $showAdditions
        ]);
    }

    public function actionPaySuccess() {
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
//            var_dump($post);
        }
        return 'Покупка билета прошла успешно!';
    }

    public function actionPayFail() {
        return 'no';
    }

    public function actionLogin() {
        $form = new LoginForm();
        if ($form->load(Yii::$app->request->post()) && $form->login()) {
            return Yii::$app->getResponse()->redirect(Url::toRoute(['/admin']));
//            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $form,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionReg() {
        $users = [
//            'info@vaturn.ru' => 'vaturnQWERTY0912'
//            'test@partner.ru' => '000000'
//            'djancord@gmail.com' => 'vvkOuu123X',
//            'lorin@sovetromantica.com' => 'm64S0wNbd1',
//            'okasitv@gmail.com' => '579MdMl23G'
//          'lucern201013@yandex.ru' => 'ap34mFLqoV',
//            'evin.sk2@mail.ru' => 'edN98Koq4M'
//            'rutoximanager@gmail.com' => 'q0n35FGep5'
//            'panda301199@gmail.com' => '20Lmf54Co9'
        ];

        $date = date('Y-m-4 H:m:s');

        foreach ($users as $login => $password) {
            $user = new User();
            $user->login = $login;
            $user->setPassword($password);
            $user->status = 1;
            $user->created_at = $date;
            $user->save(false);

            $lasId = $user->id;
//            $userRole = Yii::$app->authManager->getRole('admin');
            $userRole = Yii::$app->authManager->getRole('account');
            Yii::$app->authManager->assign($userRole, $lasId);
        }
    }

    public function actionWon() {
        $ticket = new Ticket();
        $ticket->number = 10292292702;
        $create = $ticket->saveTicket(1); //ticket_type
        if ($create) {
            $attachment = dirname(__DIR__) . '/web/doc/tickets/' . $ticket->number . '.jpg';
            if (file_exists($attachment)) {
                echo 'attached<br>';
//                $subject = 'Повторная отправка билета ' . Yii::$app->name;
//                $subject = 'Победитель розыгрыша ' . Yii::$app->name;
                $subject = 'Переоформление билета';
//                $subject = 'Покупка билета на сайте ' . Yii::$app->name;
//                $text = 'Покупка билета №' . $ticket->number . ' на сайте ' . Yii::$app->name;
//                $text = '<p>Поздравляем! Вы стали победителем еженедельного розыгрыша билетов на сайте ' . Yii::$app->params['host'] . '. Чтобы попасть на фестиваль, просто предъявите билет, прикреплённый к этому письму.</p><p>С уважением, администрация VA Turn</p>';
//                $text = '<p>Здравствуйте. Высылаем повторно билет №' . $ticket->number . '.</p>';
//                $text = 'Был переоформлен билет с Премиум балкон №32016854171 на Премиум платформа №31023811784.';
                $text = 'Переоформление билета с электронного адреса Dasha.Ulanova@yandex.ru на Dasha.Ulanova2017@yandex.ru.';
//                $text = 'Повторная отправка билета на email Khlopchur@mail.ru';

                $client = new Customer();
                $client->email = 'Sasha1972md@gmail.com';
                $sent = $client->sendEmail($subject, $text, $attachment);

                if ($sent) {
                    echo 'sent';
                    Yii::$app->mailer->compose()
                        ->setFrom([Yii::$app->params['projectEmail'] => Yii::$app->name . ' robot'])
                        ->setTo(Yii::$app->params['adminEmail'])
//                        ->setSubject('Розыгрыш билетов')
//                        ->setSubject('Повторная отправка билета')
//                        ->setSubject('Покупка билета на сайте ' . Yii::$app->name)
                        ->setSubject('Переоформление билета')
//                        ->setHtmlBody('<p>Повторная отправка билета №' . $ticket->number . '</p><p>email: ' . $client->email . '</p>')
//                        ->setHtmlBody('<p>Покупка на сайте билета №' . $ticket->number . '</p><p>email: ' . $client->email . '</p>')
                        ->setHtmlBody('<p>Переоформление билета с электронного адреса Dasha.Ulanova@yandex.ru на Dasha.Ulanova2017@yandex.ru.</p>')
//                        ->setHtmlBody('Переоформление билета с Премиум балкон №32016854171 на Премиум платформа №31023811784')
//                        ->setHtmlBody('Победитель розыгрыша ' . $client->email . '. Билет №' . $ticket->number)
//                        ->setHtmlBody('Повторная отправка билета на email Khlopchur@mail.ru')
                        ->send();
                }
            }
        }
    }

    public function actionSent() {
//        $this->view->title = 'Аниме-фестиваль VA Turn. Инструкция к билету.';
//
//        $i = 0;
//        $data = '';
//
//        while (!empty($newsLetters = Newsletter::find()->limit(10)->where(['sent' => 0])->orderBy(['id' => SORT_ASC])->all())) {
//            foreach ($newsLetters as $letter) {
//                try {
//                    $status = Yii::$app->mailer->compose('mail', [])
//                        ->setFrom([Yii::$app->params['projectEmail'] => Yii::$app->name . ' robot'])
//                        ->setTo($letter->email)
////                        ->setTo('devilcatt1@gmail.com')
//                        ->attach(dirname(__DIR__) . '/web/doc/Instruktsia_VA_Turn_2.pdf')
//                        ->setSubject('Аниме-фестиваль VA Turn. Инструкция к билету.')
//                        ->send();
//
//                    if ($status) {
//                        $user = Newsletter::findOne(['email' => $letter->email, 'sent' => 0]);
//                        if (!empty($user)) {
//                            $user->sent = 1;
//                            $user->update();
//                        }
//                        $data .= '<p>' . $letter->email . '</p>';
//                        $i++;
//                    }
//
//                } catch (\Swift_TransportException $e) {
//                }
//            }
//        }
//        return 'Отправлено ' . $i . ' контактам.<br>' . $data;
    }

    public function actionRemind() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $newsletter = new Newsletter();
            $post[$newsletter->formName()] = Yii::$app->request->post();

            if (isset($post[$newsletter->formName()]['email'])) {
                if ($newsletter->load($post)) {
                    if ($newsletter->validate()) {
                        $newsletter->email = $post[$newsletter->formName()]['email'];
                        if ($newsletter->save()) {

                            Yii::$app->mailer->compose()
                                ->setFrom([Yii::$app->params['projectEmail'] => Yii::$app->name . ' robot'])
                                ->setTo($newsletter->email)
                                ->setSubject('Подписка на трансляцию')
                                ->setHtmlBody('<p>Вы подписалиь на расслыку новостей VA turn. Теперь вы будете в курсе всех событий!</p>')
                                ->send();

                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return [
                                'status' => 1,
                                'error' => 0
                            ];
                        }
                    } else {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'status' => 0,
                            'error' => $newsletter->errors
                        ];
                    }
                }
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => 0,
            ];
        }
    }

    public function actionWriteUs() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $write = new Request(['scenario' => Request::SCENARIO_REQUEST]);
            $post[$write->formName()] = Yii::$app->request->post();

            if (isset($post[$write->formName()]['email']) && isset($post[$write->formName()]['text'])) {
                if ($write->load($post)) {
                    if ($write->validate()) {
                        $write->email = $post[$write->formName()]['email'];
                        $write->text = $post[$write->formName()]['text'];
                        $write->type_id = RequestType::WRITE_US;
                        if ($write->save()) {
                            $newsletter = Newsletter::findOne(['email' => $write->email]);
                            if (empty($newsletter)) {
                                $newsletter = new Newsletter();
                                $newsletter->email = $write->email;
                                $newsletter->save();
                            }

                            Yii::$app->mailer->compose()
                                ->setFrom([Yii::$app->params['projectEmail'] => Yii::$app->name . ' robot'])
                                ->setTo(Yii::$app->params['projectEmail'])
                                ->setSubject('Вопрос с лендинга')
                                ->setHtmlBody('<p>От: ' . $write->email . '</p><p>Сообщение: ' . $write->text . '</p>')
                                ->send();

                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return [
                                'status' => 1,
                                'error' => 0
                            ];
                        }
                    } else {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'status' => 0,
                            'error' => $write->errors
                        ];
                    }
                }
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => 0,
            ];
        }
    }

//    public function actionBuyTicket() {
//        $number = '33 635 487 978';
//        $numberString = 'Билет: ' . $number;
//
////        $imageCanva = imageCreate(827, 2362);
//        $image = imageCreateFromJPEG("images/ticket/premium_plat-2.jpg");
////        imagecopymerge($imageCanva, $image, 0, 0, 0, 0, 827, 2362, 75);
//        $color = ImageColorAllocate($image, 255, 244, 0);
//        $color2 = ImageColorAllocate($image, 33, 33, 33);
//
//        $montserrat = dirname(__DIR__) . '/web/fonts/Montserrat/Montserrat-Black.ttf';
//        $ntf = dirname(__DIR__) . '/web/fonts/NTF-Grand/NTF-Grand-Regular.ttf';
//
////        $BoundingBox1 = imagettfbbox(45, 0, $montserrat, $numberString);
////        $boyX = ceil((125 - $BoundingBox1[2]) / 2); // lower left X coordinate for text
////        $BoundingBox2 = imagettfbbox(13, 0, $montserrat, $number);
////        $girlX = ceil((107 - $BoundingBox2[2]) / 2); // lower left X coordinate for text
//
//        imagettftext($image, 45, 90, 1905, 770, $color, $montserrat, $numberString);
////        imagettftext($image, 59, 0, $girlX+445, 2165, $color2, $ntf, $number);
//        imagettftext($image, 59, 90, 2167, 400, $color2, $ntf, $number);
//
////        Header("Content-type: image/jpeg");
//
//        ImageJPEG($image,  dirname(__DIR__) . '/web/doc/tickets/55.jpg', 95);
//        ImageDestroy($image);
//    }

}
