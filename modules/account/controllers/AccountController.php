<?php

namespace app\modules\account\controllers;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
//use app\models\Newsletter;
//use app\models\Request;
use app\models\RequestType;
use yii\data\ArrayDataProvider;
use yii\web\Response;
use app\models\UserCustomer;
use app\models\Order;


class AccountController extends Controller {

    const PAGE_SIZE = 20;

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionCustomers() {
        $customers = UserCustomer::findAllByPartner();
        
        $provider = new ArrayDataProvider([
            'allModels' => $customers,
            'sort' => [
//                'attributes' => ['id', 'email'],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        return $this->render('customers', [
            'customers' => $provider
        ]);
    }
    
    public function actionOrders() {
        $orders = [];
        $customers = UserCustomer::findAllByPartner();
        if (!empty($customers)) {
            $orders = Order::findAllByUsers($customers);
        }
        
        $sum = 0;
        if (!empty($orders)) {
            foreach ($orders as $order) {
                $sum += $order->ticket->type->price;
            }
            $sum = number_format($sum / 100 * 10, 2, '.', '');
        }

        $provider = new ArrayDataProvider([
            'allModels' => $orders,
            'sort' => [
//                'attributes' => ['id', 'email'],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        return $this->render('orders', [
            'orders' => $provider,
            'sum' => $sum
        ]);
    }

//    public function actionRequestStatus() {
//        if (Yii::$app->request->isAjax) {
//            $response = false;
//
//            $id = (int)Yii::$app->request->getQueryParams()['id'];
//            $value = (int)Yii::$app->request->getQueryParams()['value'];
//
//            $requests = Request::findOne($id);
//            $requests->status = $value;
//            if ($requests->update() !== false) {
//                $response = true;
//            }
//
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return [
//                'status' => $response,
//            ];
//        }
//        Yii::$app->end();
//    }

//    public function actionDelete() {
//        if (Yii::$app->request->isAjax) {
//            $response = false;
//
//            $model = Yii::$app->request->getQueryParams()['model'];
//            $id = (int)Yii::$app->request->getQueryParams()['id'];
//
//            switch ($model) {
//                case 'request':
//                    $delete = new Request();
//                    break;
//                default:
//                    return false;
//            }
//
//            $delete = $delete->findOne($id);
//            if ($delete) {
//                if ($delete->delete() !== false) {
//                    $response = true;
//                }
//            }
//
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return [
//                'status' => $response,
//            ];
//        }
//        Yii::$app->end();
//    }


}