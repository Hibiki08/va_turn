<?php

namespace app\modules\account\assets;

use Yii;
use yii\web\AssetBundle;
use yii\web\View;


class AccountAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => View::POS_HEAD];

    public $css = [
//        'lib/jquery-ui-1.12.0/jquery-ui.css',
        'css/admin.css',
    ];
    public $js = [
//        'lib/jquery-ui-1.12.0/jquery-ui.js',
        'js/admin.js?r2',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\web\JqueryAsset'
    ];

    public function init() {
        Yii::setAlias('account', '/modules/account/assets/');
        parent::init();
    }

}
