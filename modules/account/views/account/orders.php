<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $tmClasses yii\data\ActiveDataProvider */
/* @var $rClasses yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stat row">
    <div class="stat-content">
        <h1><?php echo Html::encode($this->title); ?></h1>

        <?= GridView::widget([
            'dataProvider' => $orders,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
//            'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
            'showFooter' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id:text:№ заказа',
                [
                    'label' => 'Покупатель',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->client->email;
                    },
                ],
                [
                    'label' => 'Тип билета',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->ticket->type->name;
                    },
                    'format' => 'html',
                ],
                [
                    'label' => '№ билета',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->ticket->number;
                    },
                ],
                [
                    'label' => 'Цена билета',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->ticket->type->price;
                    },
                ],
                [
                    'label' => 'Доход',
                    'value' => function ($model, $key, $index, $column) {
                        $price = $model->ticket->type->price;
                        return number_format($price / 100 * 10, 2, '.', '');
                    },
                    'footer' => $sum
                ],
//                [
//                    'attribute'=>'charges_cash',
//                    'pageSummary'=>true,
//                ],
            ],
        ]) ?>
    </div>
</div>
