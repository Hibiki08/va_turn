<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $tmClasses yii\data\ActiveDataProvider */
/* @var $rClasses yii\data\ActiveDataProvider */

$this->title = 'Покупатели';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stat row">
    <div class="stat-content">
        <h1><?php echo Html::encode($this->title); ?></h1>

        <?= GridView::widget([
            'dataProvider' => $customers,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Email',
                    'attribute' => 'Email',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->customer->email;
                    },
                ],
            ],
        ]) ?>
    </div>
</div>
