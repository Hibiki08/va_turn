<?php

namespace app\modules\admin\controllers;

use app\models\Ticket;
use app\models\UserCustomer;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use app\models\Newsletter;
use app\models\Request;
use app\models\RequestType;
use yii\data\ArrayDataProvider;
use yii\web\Response;
use app\models\User;
use app\models\Order;
//use yii\db\Query;


class AdminController extends Controller {

    const PAGE_SIZE = 20;

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionNewsletter() {
        $newsletter = Newsletter::getAll();

        $provider = new ArrayDataProvider([
            'allModels' => $newsletter,
            'sort' => [
                'attributes' => ['id', 'email'],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        return $this->render('newsletter', [
            'emails' => $provider
        ]);
    }

    public function actionWriteUs() {
        $searchModel = new Request();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
        return $this->render('write_us', [
            'requests' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionRequestStatus() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $id = (int)Yii::$app->request->getQueryParams()['id'];
            $value = (int)Yii::$app->request->getQueryParams()['value'];

            $requests = Request::findOne($id);
            $requests->status = $value;
            if ($requests->update() !== false) {
                $response = true;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionDelete() {
        if (Yii::$app->request->isAjax) {
            $response = false;

            $model = Yii::$app->request->getQueryParams()['model'];
            $id = (int)Yii::$app->request->getQueryParams()['id'];

            switch ($model) {
                case 'request':
                    $delete = new Request();
                    break;
                default:
                    return false;
            }

            $delete = $delete->findOne($id);
            if ($delete) {
                if ($delete->delete() !== false) {
                    $response = true;
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionPartners() {
        $partners = User::getUsersWithAccountRole();

        $provider = new ArrayDataProvider([
            'allModels' => $partners,
            'sort' => [
                'attributes' => ['id', 'login'],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);
        return $this->render('partners', [
            'partners' => $provider
        ]);
    }
    
    public function actionOrders() {
        $partners = User::getUsersWithAccountRole();

        $provider = new ArrayDataProvider([
            'allModels' => $partners,
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        return $this->render('orders', [
            'partners' => $provider
        ]);
    }

    public function actionCame() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $res = false;

            $id = Yii::$app->request->post('id');
            $came = Yii::$app->request->post('value');

            if (!empty($id)) {
                $ticket = Ticket::findOne($id);
                $ticket->came = $came;
                if ($ticket->save() !== false) {
                    $res = true;
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $res,
            ];
        }
    }

    public function actionTickets() {
        $sum = 0;
        $tickets = Ticket::getBought();

        if (!empty($tickets)) {
            foreach ($tickets as $ticket) {
                $sum += $ticket->type->price;
            }
        }

        $searchModel = new Ticket();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('tickets', [
            'tickets' => $dataProvider,
            'searchModel' => $searchModel,
            'sum' => $sum
        ]);
    }
    
}