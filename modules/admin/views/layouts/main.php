<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\modules\admin\assets\AdminAsset;

AdminAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="fuelux">
<head>
    <meta charset="UTF-8">
    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo $this->title; ?></title>
    <?php $this->head() ?>
    <link rel="icon" type="image/png" href="/images/16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/images/24.png" sizes="24x24">
    <link rel="icon" type="image/png" href="/images/32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/images/48.png" sizes="48x48">
    <link rel="icon" type="image/png" href="/images/64.png" sizes="64x64">
    <link rel="icon" href="/images/favicon.ico">
</head>
<body>
<?php $this->beginBody() ?>
<?php
NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
if (Yii::$app->user->can('adminPermission')) {
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => [
            ['label' => 'Заказы',
                'url' => ['/admin/tickets'],
            ],
            ['label' => 'Партнёры',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Список', 'url' => ['/admin/partners']],
                    ['label' => 'Заказы', 'url' => ['/admin/orders']],
                ],
            ],
            ['label' => 'Заявки',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Связаться с нами', 'url' => ['/admin/write-us']],
                ],
            ],
            ['label' => 'Рассылка',
                'url' => ['#'],
                'items' => [
                    ['label' => 'Список email-адресов', 'url' => ['/admin/newsletter']],
                ],
            ],
            '<li id="exit">'
            . Html::beginForm(['/logout'], 'post')
            . Html::submitButton(
                'Выйти',
                ['class' => 'btn btn-secondary col-lg-12']
            )
            . Html::endForm()
            . '</li>'
        ],
    ]);
} 
NavBar::end();
?>
<div class="container content">
    <div class="row-fluid">
        <div>
            <?php echo $content ?>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
