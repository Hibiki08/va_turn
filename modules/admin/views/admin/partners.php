<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
//use app\components\StatisticsMenu;

/* @var $this yii\web\View */
/* @var $tmClasses yii\data\ActiveDataProvider */
/* @var $rClasses yii\data\ActiveDataProvider */

$this->title = 'Партнёры > Список';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stat row">
    <div class="stat-content">
        <h1><?php echo Html::encode($this->title); ?></h1>

        <?= GridView::widget([
            'dataProvider' => $partners,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id:text:ID',
                'login:text:Email',
                [
                    'label' => 'Ссылка',
                    'value' => function($model) {
                        return '<code>' . Yii::$app->urlManager->hostInfo . Url::to(['/', 'link' => $model->link]) . '</code>';
                    },
                    'format' => 'raw',
                ]
            ],
        ]) ?>
    </div>
</div>
