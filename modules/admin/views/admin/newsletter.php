<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
//use app\components\StatisticsMenu;

/* @var $this yii\web\View */
/* @var $tmClasses yii\data\ActiveDataProvider */
/* @var $rClasses yii\data\ActiveDataProvider */

$this->title = 'Список email-адресов';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stat row">
    <div class="stat-content">
        <h1><?php echo Html::encode($this->title); ?></h1>
        
        <?= GridView::widget([
            'dataProvider' => $emails,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id:text:ID',
                'email:text:Email',
            ],
        ]) ?>
    </div>
</div>
