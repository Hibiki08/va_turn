<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
//use app\components\StatisticsMenu;

/* @var $this yii\web\View */
/* @var $tmClasses yii\data\ActiveDataProvider */
/* @var $rClasses yii\data\ActiveDataProvider */

$this->title = 'Партнёры > Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stat row">
    <div class="stat-content">
        <h1><?php echo Html::encode($this->title); ?></h1>

        <?= GridView::widget([
            'dataProvider' => $partners,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id:text:ID',
                [
                    'label' => 'Партнёр',
                    'value' => function($model) {
                        return $model->login;
                    }
                ],
                [
                    'label' => 'Клиенты',
                    'value' => function($model) {
                        if (isset($model->orders)) {
                            $users = [];
                            foreach ($model->orders as $order) {
                                $users[] = $order->customer->email;
                            }
                            return implode('<br>', $users);
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'label' => 'Выплатить',
                    'value' => function($model) {
                        $sum = 0;

                        if (isset($model->orders)) {
                            foreach ($model->orders as $order) {
                                if (isset($order->customer->orders)) {
                                    foreach ($order->customer->orders as $ticket) {
                                        $price = $ticket->ticket->type->price;
                                        $sum += $price;
                                    }
                                }
                            }
                            if ($sum > 0) {
                                return $sum / 100 * 10;
                            }
                        }
                        return 0;
                    },
                ]
            ],
        ]) ?>
    </div>
</div>
