<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\Url;
//use app\components\StatisticsMenu;

/* @var $this yii\web\View */
/* @var $tmClasses yii\data\ActiveDataProvider */
/* @var $rClasses yii\data\ActiveDataProvider */

$this->title = 'Связаться с нами';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stat row">
    <div class="stat-content">
        <h1><?php echo Html::encode($this->title); ?></h1>

        <?= GridView::widget([
            'dataProvider' => $requests,
            'summary' => '',
            'filterModel' => $searchModel,
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id:text:ID',
                'email:text:Email',
                'text:text:Сообщение',
                [
                    'label' => 'Дата',
                    'attribute' => 'created_at',
                    'filter' => '<div class="form-group">' . DatePicker::widget([
                            'language' => 'ru',
                            'model' => $searchModel,
                            'attribute' => 'created_at',
                            'dateFormat' => 'dd-MM-yyyy',
                            'options' => [
                                'class' => 'form-control'
                            ]
                        ]). '</div>',
                    'format' =>  ['date', 'dd-MM-Y HH:mm:ss']
                ],
                [
                    'label' => 'Статус',
                    'attribute' => 'status',
                    'value' => function ($model, $key, $index, $column) {
//                        return $model->status ? 'Обработано' : 'Не обработано';
                        return '<div class="btn-group-vertical">
                            <a class="btn btn-primary btn-xs btn-activate" data-value="' . ($model->status ? 0 : 1) . '" data-id="' . $model->id . '">
                                <span>' . ($model->status ? 'Обработано' : 'Не обработано') . '</span>
                                <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </a>
                            <a class="btn btn-danger btn-xs btn-delete" data-id="' . $model->id . '">
                                Удалить
                                <div class="progress progress-striped active" style="border-radius: 0;margin: 0;height: 3px; display: none">
                                    <div class="progress-bar progress-bar-danger" style="width: 100%"></div>
                                </div>
                            </a>
                        </div>';
                    },
                    'format' => 'raw',
                ]
            ],
        ]) ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-activate').click(function () {
            var $this = $(this);
            var value = $this.attr('data-value');
            var id = $this.data().id;
            $this.find('.progress').show();
            $.ajax({
                url: '<?php echo Url::to('request-status'); ?>',
                type: 'get',
                dataType: 'json',
                data: {id: id, value: value},
                success: function (response) {
                    if (response.status == true) {
                        if (value == 1) {
                            $this.find('span').text('Обработана');
                            $this.attr('data-value', 0);
                        } else {
                            $this.find('span').text('Не обработана');
                            $this.attr('data-value', 1);
                        }
                    }
                    $this.find('.progress').hide();
                },
                error: function () {
                    $this.find('.progress').hide();
                }
            });
        });

        $('.btn-delete').click(function () {
            var agree = confirm('Вы действительно хотите удалить этот пункт?');
            if (agree) {
                var $this = $(this);
                deleteAjax($this, '<?php echo Url::to('delete'); ?>', 'request');
            }
        });
    });
</script>
