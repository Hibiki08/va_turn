<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
//use app\components\StatisticsMenu;

/* @var $this yii\web\View */
/* @var $tmClasses yii\data\ActiveDataProvider */
/* @var $rClasses yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stat row">
    <div class="stat-content">
        <h1><?php echo Html::encode($this->title); ?></h1>

        <?= GridView::widget([
            'dataProvider' => $tickets,
            'filterModel' => $searchModel,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'showFooter' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id:text:ID билета',
                [
                    'attribute' => 'ticketOrder',
                    'label' => '№ Заказа',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->order->id;
                    },
                ],
                [
                    'attribute' => 'ticketNumber',
                    'label' => '№ билета',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->number;
                    },
                    'format' => 'html',
                ],
                [
                    'attribute' => 'ticketName',
                    'label' => 'Покупатель',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->order->client->name;
                    },
                ],
                [
                    'attribute' => 'ticketEmail',
                    'label' => 'Email',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->order->client->email;
                    },
                ],
                [
                    'label' => 'Тип билета',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->type->name;
                    },
                    'format' => 'html',
                ],
                [
                    'label' => 'Цена билета',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->type->price;
                    },
                    'footer' => $sum
                ],
                [
                    'label' => 'Пришёл',
                    'value' => function ($model, $key, $index, $column) {
                        return Html::checkbox('came', $model->came, ['value' => $model->came, 'class' => 'came', 'data-id' => $model['id']]);
                    },
                    'format' => 'raw',
                ],
            ],
        ]) ?>
    </div>
</div>
