<?php

namespace app\modules;

use Yii;
use yii\filters\AccessControl;
use yii\base\Module;


class AccountModule extends Module {

    public $controllerNamespace = 'app\modules\account\controllers';
    
//    public function beforeAction($action) {
//
//        if (!parent::beforeAction($action)) {
//            return false;
//        }
//
////        if (!Yii::$app->user->isGuest) {
////            return true;
////        }
////        else {
////            Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
////            //для перестраховки вернем false
////            return false;
////        }
//    }

    public function init() {
        $this->setViewPath(Yii::$app->basePath . '/modules/account/views');
        $this->layout = "main.php";
        parent::init();
    }

}