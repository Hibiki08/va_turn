<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

yii\web\YiiAsset::register($this);
yii\bootstrap\BootstrapAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="icon" type="image/png" href="/images/16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/images/24.png" sizes="24x24">
    <link rel="icon" type="image/png" href="/images/32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/images/48.png" sizes="48x48">
    <link rel="icon" type="image/png" href="/images/64.png" sizes="64x64">
    <link rel="icon" href="/images/favicon.ico">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div style="max-width: 1140px; margin: 0 auto">
    <?php echo $content; ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
