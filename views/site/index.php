<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\models\Supplement;

/* @var $this yii\web\View */

$this->title = 'VA turn - Главное аниме-событие года';
?>
<div id="error-block">
    <div class="text"></div>
    <div class="tail"></div>
</div>
<div class="popup message">
    <div class="width">
        <div class="block-message">
            <div class="text">
                <div class="text-success">Ваша почта успешно отправлена!</div>
                <p>Спасибо за подписку на новости! Никакого спама, только интересные акции и розыгрыши!</p>
                <p>Мы пришлем вам на почту уведомление о начале трансляции на youtube.</p>
                <div class="btn">ok!</div>
            </div>
        </div>
    </div>
</div>
<?php if (Yii::$app->request->get('pay') == 'success') { ?>
<div class="popup pay-success">
    <div class="width">
        <div class="block-form">
            <div class="form-popup">
                <div class="title">
                    <div>Покупка&nbsp;билета</div>
                </div>
                <div class="form-cont">
                    <div class="success">Ваш заказ успешно оформлен!</div>
                    <div class="box">
                        <div class="part left">
                            <p>Билет отправлен на указанный email: <span><?php echo $customer_email; ?></span></p>
                            <p>Вы можете распечатать его или предъявить на входе в электронном виде.</p>
                            <div class="thank">Благодарим за покупку!</div>
                        </div>
                        <div class="part right">
                            <p>Если билет не пришел на указанную почту, пожалуйста, <span>свяжитесь с нами</span> любым удобным способом:</p>
                            <div class="contacts">
                                <a href="tel:<?php echo Yii::$app->params['phone']; ?>"><i class="mdi mdi-cellphone-android"><?php echo Yii::$app->params['phone']; ?></i></a>
                                <a href="mailto:<?php echo Yii::$app->params['projectEmail']; ?>"><i class="mdi mdi-email-outline"><?php echo Yii::$app->params['projectEmail']; ?></i></a>
                                <a class="anchor" href="#footer"><i class="mdi mdi-pencil-box-outline">форма обратной связи</i></a>
                                <a target="_blank" href="https://vk.com/vaturn"><i class="mdi mdi-vk-box">задайте вопрос в вк</i></a>
                            </div>
                        </div>
                    </div>
                    <div class="btn" id="buy-more">Купить еще один билет</div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="popup buy-ticket">
    <div class="width">
        <div class="block-form">
            <div class="form-popup">
                <div class="title">
                    <div>Покупка&nbsp;билета</div>
                </div>
                <div class="form-cont">
                    <?php $form = ActiveForm::begin([
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => true,
                        'options' => [
                            'id' => 'form_buy_ticket',
                        ]
                    ]);?>
                        <div class="description">
                            <div class="about">
                                <div class="header">
                                    <div class="logo"><img src="/images/logo.svg" alt="va turn"></div>
                                    <div>Fest</div>
                                </div>
                            </div>
                            <div class="choose">
                                <?php if (!empty($tickets)) { ?>
                                    <?php if ($first) { ?>
                                <div class="price"><span class="int"><?php echo $first->price; ?></span><span> ₽</span></div>
                                    <?php echo $form->field($ticket_form, 'ticket_id', [
                                        'template' => '{error}{input}',
                                    ])->hiddenInput([
                                        'value' => $first->id
                                    ]); ?>
                                        <div id="tickets">
                                            <div class="active">Билет <?php echo $first->name; ?>
                                                <span class="rub"><?php echo $first->price; ?></span>
                                              <span class="price-ticket"><?php echo $first->price; ?> <span>р.</span></span>
                                            </div>
                                            <ul class="tickets-list">
                                                <?php foreach ($tickets as $ticket) { ?>
                                                    <li <?php echo $ticket->left == 0 ? 'class="disabled"' : ''; ?> data-id="<?php echo $ticket->id; ?>" data-price="<?php echo $ticket->price; ?>">Билет <?php echo $ticket->name; ?><span class="rub"><?php echo $ticket->price; ?></span><?php echo $ticket->left == 0 ? '<p class="sold-out">Распроданы</p>' : ''; ?></li>
                                                <?php } ?>
                                            </ul>
                                        </div>

                                        <div id="additionally">
                                            <span class="heading">Дополнительно:</span>
                                            <?php if ($showAdditions) { ?>
                                            <?php if (!empty($supplement)) { ?>
                                            <ul>
                                                <?php foreach ($supplement as $suppl) { ?>
                                                    <?php if ($suppl->id == Supplement::PHOTO_ID || $suppl->id == Supplement::BOX_ID) {?>
                                                    <li class="common arrow-common">
                                                        <div class="counter">
                                                            <span class="arrow-down mdi mdi-menu-left"></span>
                                                            <span class="quantity"><?php echo $form->field($ticket_form, $suppl->key_word, [
                                                                    'template' => '{input}',
                                                                ])->label(false); ?></span>
                                                            <span class="arrow-up mdi mdi-menu-right"></span>
                                                        </div>
                                                        <span class="description"><?php echo $suppl->title; ?></span>
                                                        <span class="summ">
                                                            <span data-price="<?php echo $suppl->price; ?>">0</span> р.
                                                        </span>
                                                        <?php if (!empty($suppl->description)) { ?>
                                                            <span class="mdi mdi-help-circle"></span>
                                                            <div class="message">
                                                                <div class="help-block"><?php echo $suppl->description; ?></div>
                                                            </div>
                                                        <?php } ?>
                                                    </li>
                                                    <?php } ?>
                                                <?php if ($suppl->id == Supplement::EARLY_ID || $suppl->id == Supplement::TOO_EARLY_ID ||
                                                        $suppl->id == Supplement::BEHIND_ID) { ?>
                                                        <li class="common checkbox-common">
                                                                <?php echo $form->field($ticket_form, $suppl->key_word, [
                                                                    'template' => '<label>{input}<span class="checkbox-custom mdi"></span><span class="label description">' . $suppl->title . '</span><span class="summ"><span data-price="' . $suppl->price . '">' . $suppl->price . '</span> р.</span></label>',
                                                                ])->checkbox([
                                                                    'class' => 'checkbox'
                                                                ], false); ?>
                                                            <?php if (!empty($suppl->description)) { ?>
                                                                <span class="mdi mdi-help-circle"></span>
                                                                <div class="message">
                                                                    <div class="help-block"><?php echo $suppl->description; ?></div>
                                                                </div>
                                                            <?php } ?>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            </ul>
                                            <?php } ?>
                                        <?php } else { ?>
                                                <p style="color: #1c002d;padding-top: 15px;">Недоступно</p>
                                            <?php } ?>
                                        </div>

                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="client-data">
                            <h3>Оформление заказа:</h3>
                                <?php echo $form->field($ticket_form, 'name', [
                                    'template' => '<i class="mdi mdi-account"></i>{error}{input}',
                                ])->input('text', [
                                    'class' => 'focus',
                                    'placeholder' => 'Фамилия и имя*'
                                ]); ?>
                                <?php echo $form->field($ticket_form, 'phone', [
                                    'template' => '<i class="mdi mdi-phone"></i>{error}{input}',
                                ])->input('text', [
                                    'class' => 'phone-mask',
                                    'placeholder' => 'Телефон*'
                                ]); ?>
                                <?php echo $form->field($ticket_form, 'date', [
                                    'template' => '<i class="mdi mdi-cake-variant"></i><i class="mdi mdi-calendar-text"></i>{error}{input}',
                                ])
                                ->widget(DatePicker::class, [
                                    'dateFormat' => 'dd-MM-yyyy',
                                    'language' => 'ru',
                                    'options' => [
                                        'placeholder' => Yii::$app->formatter->asDate($ticket_form->date),
                                        'class' => '',
                                        'autocomplete' => 'off'
                                    ],
                                    'clientOptions' => [
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'yearRange' => '1900:2003',
                                    ]
                                ])->textInput([
                                        'placeholder' => 'Дата рождения',
                                        'autocomplete' => 'off',
                                        'class' => 'birthday',
                                    ]);
                                ?>
                            <h4>Куда прислать билет?</h4>
                                <?php echo $form->field($ticket_form, 'email', [
                                    'template' => '<i class="mdi mdi-email"></i>{error}{input}',
                                ])->input('text', [
                                    'placeholder' => 'Email*',
                                ]); ?>
                                <?php echo $form->field($ticket_form, 'subscribe', [
                                    'template' => '<label>{input}<span class="checkbox-custom mdi"></span><span class="label">Хочу получать напоминания и новости VA fest</span></label>',
                                ])->checkbox([
                                    'class' => 'checkbox'
                                ], false); ?>
                            <div class="sub">
                                <p>Нажимая на кнопку, вы соглашаетесь с <a target="_blank" href="/doc/oferta.pdf">Договором публичной оферты</a> и с <a target="_blank" href="/doc/oplata.pdf">Условиями оплаты онлайн</a>.</p>
                            </div>
                            <div class="btn" id="pay">Перейти к оплате</div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="popup enter">
    <div class="width">
        <div class="block-form">
            <div class="form-popup">
                <div class="title">
                    <div>Партнёры</div>
                </div>
                <div class="form-cont">
                    <form>
                        <h3>Вход в личный кабинет</h3>
                        <div class="block-input">
                            <i class="mdi mdi-account"></i>
                            <input type="text" placeholder="Логин" name="login">
                        </div>
                        <div class="block-input">
                            <i class="mdi mdi-key-variant"></i>
                            <i class="mdi mdi-eye"></i>
                            <input type="password" placeholder="Пароль" name="password">
                        </div>
                        <div class="sub">
                            <div>
                                <i class="mdi mdi-comment-question-outline"></i>
                                <span>У меня нет логина / пароля</span>
                                <i class="mdi mdi-menu-down"></i>
                            </div>
                            <p>Пароль и логин от личного кабинета выдается всем партнерам-рекламодателям. Для получения доступа свяжитесь с администратором фестиваля по телефону <span>8 (800) 707-99-83</span> или напишите на почту <span>oltar.d@yandex.ru</span></p>
                        </div>
                        <div class="btn" id="enter">Войти</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="menu-open">
    <div class="width">
        <div class="logo"><a href="/"><img src="/images/logo.svg" alt="va turn"></a></div>
        <div class="items">
            <div class="cross"><i class="mdi mdi-close"></i></div>
            <div class="list">
                <ul>
                    <li><a href="#members">Участники</a></li>
                    <li><a href="#program">Программа</a></li>
                    <li><a href="#music">Музыканты</a></li>
                    <li><a href="#fair">Ярмарка</a></li>
                    <li><a href="#block-tickets">Билеты</a></li>
                    <li><a href="#journalists">Журналисты</a></li>
                    <li><a href="#thanks">Благодарности</a></li>
                </ul>
<!--                <div class="sub">-->
<!--                    <div class="enter">Вход для партнеров</div>-->
<!--                    <a href="">Благодарности</a>-->
<!--                </div>-->
            </div>
            <div class="when">
                <div class="wh">
                    <div class="title">Palazzo</div>
                    <span>г. Воронеж,</span>
                    <span>ул. Московский Проспект,</span>
                    <span>дом 9, клуб Palazzo</span>
                    <div class="title">23 февраля</div>
                    <span>Суббота, с 16:00 до 21:00</span>
                </div>
            </div>
        </div>
    </div>
    <div id="map_canvas"></div>
</div>
<header class="header">
    <div class="width">
        <div class="logo"><a href="/"><img src="/images/logo.svg" alt="va turn"></a></div>
        <div class="info">
            <a class="phone" href="tel:8 800 707 99 83">8 800 707 99 83</a>
            <a href="<?php echo Url::to(['/login']); ?>" class="enter">Вход&nbsp;для&nbsp;партнеров</a>
            <div class="buy">Купить билет</div>
            <nav>
                <span>m<img src="/images/hamburger.svg" alt="hamburger">nu</span>
            </nav>
        </div>
    </div>
</header>
<main>
    <div class="introduce">
        <div class="width">
            <div class="int-1">
                <h1>Главное аниме-событие года</h1>
                <span class="name">VA&nbsp;turn</span>
                <div class="button-block buy">
                    <div class="button">Купить билет</div>
                </div>
                <div class="where">
                    <span class="date">23/02/2019</span>
                    <span class="city">Воронеж, Palazzo</span>
                </div>
                <div class="soc">
                    <a href="https://vk.com/vaturn" target="_blank"><i class="mdi mdi-vk-circle"></i></a>
                    <a href="https://www.youtube.com/channel/UCUiZZKE5ngQ14PFHaoT2gfA" target="_blank"><i class="mdi mdi-youtube-play"></i></a>
                </div>
            </div>
            <div class="int-2">
                <div class="text">Fest
                </div>
                <div class="video">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/n6WaNoewb1I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="members" id="members">
        <div class="width">
            <div class="title-block">
                <div>Участники</div>
            </div>
            <div class="slider-members">
                <div class="item">
                    <img src="/images/ankord.jpg" class="background">
                    <div class="description">
                        <div class="ava">
                            <img src="/images/ankord-mini.png" alt="ankord">
                            <div class="name">Анкорд</div>
                        </div>
                        <div class="text-block">
                            <div class="title">
                                <span>Самый известный даббер России,<br>основатель проекта AniDub</span>
                            </div>
                            <div class="text">Даббер-легенда и просто весёлый человек. Помимо ведения проекта профессиональной озвучки AniDub, также работает на американскую компанию crunchroll (перевод и релиз аниме для всего мира). Озвучил лично более 1000 аниме-серий, более 10 лет непрерывной работы. Его шутки стали мемами. От него можно зарядиться позитивным настроением, поэтому его часто зовут на разные шоу и&nbsp;фесты.</div>
                        </div>
                    </div>
                </div>
                <!--<div class="item">
                    <img src="/images/nika.jpg" class="background">
                    <div class="description">
                        <div class="ava">
                            <img src="/images/nika-mini.png" alt="nika lenina">
                            <div class="name">Ника<br>Ленина</div>
                        </div>
                        <div class="text-block">
                            <div class="title">
                                <span>Эстрадная певица и душевный даббер<br>команды AniDub</span>
                            </div>
                            <div class="text">Она может менять свой голос в широком диапазоне, что позволяет ей имитировать разные женские голоса. Основное её занятие – перепевка песен из аниме на русский язык. Но самое главное... На&nbsp;всю Россию всего несколько человек, которые делают это качественно.<br>Если вы решите послушать японскую песню на русском языке, то&nbsp;легко попадёте на её канал на Ютубе (более 258 000 подписчиков). По факту, она &ndash; Живой Лейбл.</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/images/soerov.jpg" class="background">
                    <div class="description">
                        <div class="ava">
                            <img src="/images/soerov-mini.png" alt="soerov">
                            <div class="name">Кирилл<br>Соеров</div>
                        </div>
                        <div class="text-block">
                            <div class="title">
                                <span>Ведущий самого популярного аниме-влога на Youtube (более 625 тыс. постоянных подписчиков)</span>
                            </div>
                            <div class="text">Кирилл очень весёлый человек. Его канал на Youtube – это набор лучших комедийных моментов из аниме и классный монтаж. Он снимает обзоры и новости аниме и манги, тематические аниме-топы и путешествия по Японии. У него можно будет взять автограф, пожать руку или обняться с ним.</div>
                        </div>
                    </div>
                </div>-->
                <div class="item">
                    <img src="/images/saya-2.jpg" class="background">
                    <div class="description">
                        <div class="ava">
                            <img src="/images/saya-mini.png" alt="saya skarlet">
                            <div class="name">Сая<br>Скарлет</div>
                        </div>
                        <div class="text-block">
                            <div class="title">
                                <span>Влада Корунная – девочка, которая засветилась на японских телеканалах</span>
                            </div>
                            <div class="text">Русская Хатсуне Мику! Она способна имитировать голос 3D идола. На сегодншний день Сая Скарлет выступает не только в Японии, но и по всей Азии. Исполнит самые заводные песни японского идола!</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/images/jackie.jpg" class="background">
                    <div class="description">
                        <div class="ava">
                            <img src="/images/jackie-mini.png" alt="jackie-o">
                            <div class="name">Jackie-O</div>
                        </div>
                        <div class="text-block">
                            <div class="title">
                                <span>Пожалуй, самый известный каверист по аниме-остам в&nbsp;России</span>
                            </div>
                            <div class="text">Вокалист и даббер проекта Anistar. У него десятки тысяч поклонников. Его творчество посвящено художественным адаптациям на русский язык песен из аниме и игр. Наполнит фестиваль драйвом и настоящим роком!</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/images/fox.jpg" class="background">
                    <div class="description">
                        <div class="ava">
                            <img src="/images/fox-mini.png" alt="tarelko">
                            <div class="name">Kawaii Fox</div>
                        </div>
                        <div class="text-block">
                            <div class="title">
                                <span>Один из самых известных блоггеров с более 800 тыс подписчиков.</span>
                            </div>
                            <div class="text">Александра Лис, она же Kawaii Fox, крайне миленькая и очаровательная девушка, с которой вы будете фотографироваться на Полароид, а та же увидите её удивительное преображение на сцене.</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/images/petya.jpg" class="background">
                    <div class="description">
                        <div class="ava">
                            <img src="/images/petya-mini.png" alt="rimus">
                            <div class="name">Петя Коврижных</div>
                        </div>
                        <div class="text-block">
                            <div class="title">
                                <span>Московский актёр, ведущий мероприятия VA Turn.</span>
                            </div>
                            <div class="text">Он засветился в десятках картин, одна из которых известный телесериал «Молодёжка». Персонаж - молодой хоккеист «Березин». Выступает в  дуэте с Анкордом.</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/images/unfry.jpg" class="background">
                    <div class="description">
                        <div class="ava">
                            <img src="/images/unfry-mini.png" alt="rimus">
                            <div class="name">UnFry</div>
                        </div>
                        <div class="text-block">
                            <div class="title">
                                <span>Взрывная рок-группа</span>
                            </div>
                            <div class="text">Побывав даже на сцене Старкона, они едут к нам с уникальным, действительно драйвовым репертуаром. Вас ожидают как всемирно известные каверы, так и авторская музыка.</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="/images/toki.jpg" class="background">
                    <div class="description">
                        <div class="ava">
                            <img src="/images/toki-mini.png" alt="rimus">
                            <div class="name">TokiDoki</div>
                        </div>
                        <div class="text-block">
                            <div class="title">
                                <span>Взрывная рок-группа</span>
                            </div>
                            <div class="text">Единственная в России рок-группа, по-настоящему исполняющая японские песни.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="arrows"></div>
        </div>
    </div>
    <div class="block program" id="program">
        <div class="width">
            <div class="title-block">
                <div>Программа</div>
            </div>
            <div class="pr-block">
                <div class="pr pr-1">
                    <div>
                        <h2>VA Turn (Ви Эй Тёрн)</h2>
                        <span class="explain">– это не просто вечеринка или обычное аниме-пати. Это целая серия мероприятий, которая начинается в Москве, а дальше идет длинным галопом по России!</span>
                    </div>
                    <p>Собрать известных людей аниме-индустрии это классно. Но не это главное. Что важнее, так это человеческая душа. Всем нам, порой, одиноко, скучно и мы не знаем, что делать. Если вы переживали или переживаете нечто подобное, то VA Turn – это для вас! Потому что... </p>
                </div>
                <div class="pr pr-2">
                    <p>В этот раз для VA Turn мы арендовали самый люксовый клуб в Воронеже. Как только вы зайдёте внутрь, вас сразу встретят наши прекрасные Обнимашечные Горничные и популярный блоггер Александра Лис, с которой вы сможете сделать Фото на Полароид.</p>
                    <figure><img src="/images/pr-2.1.jpg" alt="huge"></figure>
                </div>
                <div class="pr pr-3">
                    <figure><img src="/images/pr-3.1.jpg" alt="huge"></figure>
                    <div>
                        <p>Душевная атмосфера, качественный звук и яркие исполнители — это то, что вас ждёт Va Turn. У нас не будет длинных скучных речей, потому что всё начнётся с нереально драйвовой живой музыки!</p>
                        <p>Мы здесь, чтобы дать вам настоящий драйв, чтобы от сцены вы не смогли оторвать глаз!</p>
                    </div>
                </div>
                <div class="pr pr-4">
                    <div class="pr-4-cont">
                        <div>
                            <p>Аатмосфера мощного концерта из звёзд аниме-индустрии с настоящими японскими вкусняшками!</p>
                            <h2>VA Turn Fest</h2>
                            <span class="explain">– это обнимашки, действительно крутые ведущие, выступление любимых исполнителей, а&nbsp;также душевная атмосфера! Реальное лекарство от&nbsp;нереальной скуки!</span>
                        </div>
                        <div>
                            <figure><img src="/images/pr-4.1.jpg" alt="huge"></figure>
                            <div class="time-block">
                                <div class="when">
                                    <span>Стартуем в</span>
                                    <div class="text">16:00</div>
                                </div>
                                <div class="button-block buy">
                                    <div class="button"><span>Купить билет</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block music" id="music">
        <div class="width">
            <div class="title-block">
                <div>Музыканты</div>
            </div>
            <div class="msc-block">
                <div class="msc msc-1">
                    <div class="description">
                        <h2>Jackie-O</h2>
                        <p>Каверист, который первым стал перепевать опенинги к аниме на русском языке. Споёт самые "заводные" песни в рок стиле.</p>
                    </div>
                    <div class="border">
                        <div class="video">
                            <iframe width="650" height="365" src="https://www.youtube.com/embed/RzE5tzSyV5w?&rel=0&showinfo=0&iv_load_policy=3" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="msc msc-2">
                    <div class="border">
                        <div class="video">
                            <iframe width="650" height="365" src="https://www.youtube.com/embed/0khu1byIv8E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="description">
                        <h2>Saya<br>Scarlet</h2>
                        <p>Японский поп-идол русского происхождения. Споёт голосом Хатсуне Мику. А ещё она очень кавайная)</p>
                    </div>
                </div>
                <div class="msc msc-3">
                    <div class="border">
                        <div class="video">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/JhjdTuLKUVU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="description">
                        <h2>UnFry</h2>
                        <p>Драйвовая рок-группа, поющая песни на английском. Особая программа выступления только для VA Turn Воронеж.</p>
                    </div>
                </div>
                <div class="more">
                    <div class="title">Но и это ещё не всё!</div>
                    <p>TokiDoki - Единственная в России группа, полностью переигрывающая OST'ы из аниме в рок-стиле.</p>
                    <!--<p>Вы увидите впечатляющие аниме-AMV на&nbsp;большом экране и&nbsp;услышите любимые опенинги!</p>-->
                </div>
                <div class="rock-block">
                    <div class="rock">
                        <h2>TokiDoki</h2>
                        <img src="/images/tokidoki.jpg" alt="rock">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block fair" id="fair">
        <div class="width">
            <div class="title-block">
                <div>Ярмарка</div>
            </div>
            <div class="fair-block">
                <h3>Только на VA Turn - Настоящие японские закуски, напитки и сладости!</h3>
                <div class="prod prod-1">
                    <div class="slider-block">
                        <div class="slider-products">
                            <img src="/images/fair-1.1.jpg">
                            <img src="/images/fair-1.2.jpg">
                            <img src="/images/fair-1.3.jpg">
                            <img src="/images/fair-1.4.jpg">
                            <img src="/images/fair-1.5.jpg">
                        </div>
                        <div class="arrows"></div>
                    </div>
                    <div class="description">
                        <div class="title">Специально приглашённый магазин японских сладостей j-by.ru!</div>
                        <p>Напитки от 100 рублей за баночку. Сладости от 50 рублей. Теперь вы сможете попробовать всё, что давно хотели!</p>
                    </div>
                </div>
                <div class="prod prod-2">
                    <div class="description">
                        <div class="title">Ну и конечно же мерч!</div>
                        <p>Атрибутика, наклеечки, японские канцтовары и&nbsp;другие приятные мелочи нашей жизни!</p>
                        <p>На VA Turn вы можете забыть про китайский нелегальный контрафакт, которым переполнены все аниме-фесты! У нас лицензионная Япония и Корея с&nbsp;настоящими сертификатами качества от&nbsp;таможенного союза!</p>
                    </div>
                    <figure>
                        <img src="/images/fair-2.jpg">
                    </figure>
                </div>
            </div>
        </div>
    </div>
    <div class="block tickets" id="block-tickets">
        <div class="block-slider">
            <div class="slider-tickets">
                <div class="item standart">
                    <div class="width">
                        <div class="title-block">
                            <div>Билеты</div>
                        </div>
                        <div class="ticket-block">
                            <div class="name tick">
                                <div class="title">Сomfort</div>
                                <div class="sub">Чёрный браслет</div>
                                <div class="places">Сomfort всего <span>920</span> мест</div>
                                <div class="button-block buy-ticket" data="standart">
                                    <div class="button buy">Купить Сomfort</div>
                                </div>
                            </div>
                            <div class="description tick">
                                <div class="price"><span>650</span><span>руб.</span></div>
                                <div class="sub">Места в центре зала перед сценой</div>
                                <div class="list">
                                    <ul>
                                        <li>Радостные обнимашки с красивой девушкой на входе;</li>
                                        <li>Программа фестиваля в руки;</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item comfort">
                    <div class="width">
                        <div class="title-block">
                            <div>Билеты</div>
                        </div>
                        <div class="ticket-block">
                            <div class="name tick">
                                <div class="title">Premium</div>
                                <div class="sub">Жёлтый браслет</div>
                                <div class="places">Premium всего <span>24</span> места</div>
                                <div class="button-block buy-ticket" data="standart">
                                    <div class="button buy">Купить Premium</div>
                                </div>
                            </div>
                            <div class="description tick">
                                <div class="price"><span>1 500</span><span>руб.</span></div>
                                <div class="sub">Места на мягких диванчиках справа или слева от сцены</div>
                                <div class="list">
<!--                                    <div>Все возможности Сomfort и дополнительно:</div>-->
                                    <ul>
                                        <li>Радостные обнимашки с красивой девушкой на входе;</li>
                                        <li>Программа фестиваля в руки;</li>
                                        <li>Фото на Полароид с няшной блоггершей Александрой Лис;</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item premium">
                    <div class="width">
                        <div class="title-block">
                            <div>Билеты</div>
                        </div>
                        <div class="ticket-block">
                            <div class="name tick">
                                <div class="title">VIP</div>
                                <div class="sub">Белый браслет</div>
                                <div class="places">
                                    <div>VIP всего <span>12</span> мест</div>
                                </div>
                                <div class="button-block buy-ticket" data="standart">
                                    <div class="button buy">Купить VIP</div>
                                </div>
                            </div>
                            <div class="description tick">
                                <div class="price"><span>2500</span><span>руб.</span></div>
                                <div class="sub">Места на мягких диванчиках за столиком на огороженной платформе в центре зала</div>
                                <div class="list">
<!--                                    <div>Все возможности Premium и дополнительно:</div>-->
                                    <ul>
                                        <li>Радостные обнимашки с красивой девушкой на входе;</li>
                                        <li>Программа фестиваля в руки;</li>
                                        <li>Фото на Полароид с няшной блоггершей Александрой Лис;</li>
                                        <li>Меню ярмарки j-by и официантка в наряде горничной, обслуживающая вас;</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item vip">
                    <div class="width">
                        <div class="title-block">
                            <div>Билеты</div>
                        </div>
                        <div class="ticket-block">
                            <div class="name tick">
                                <div class="title">President</div>
                                <div class="sub">Серый браслет</div>
                                <div class="places">
                                    <div>President <span>1</span> билет на компанию из 6 человек</div>
                                </div>
                                <div class="button-block buy-ticket" data="standart">
                                    <div class="button buy">Купить President</div>
                                </div>
                            </div>
                            <div class="description tick">
                                <div class="price"><span>12 000</span><span>руб.</span></div>
                                <div class="sub">Шесть мест за огороженным столиком в ложе второго этажа (2000р. с человека)</div>
                                <div class="list">
<!--                                    <div>Все возможности Standart и дополнительно:</div>-->
                                    <ul>
                                        <li>Радостные обнимашки с красивой девушкой на входе;</li>
                                        <li>Программа фестиваля в руки;</li>
                                        <li>Фото на Полароид с няшной блоггершей Александрой Лис;</li>
                                        <li>Меню ярмарки j-by и официантка в наряде горничной, обслуживающая вас;</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="move">
                <div class="width">
                    <div class="dots"></div>
                    <div class="arrows"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="block partners">
        <div class="width">
            <div class="title-block">
                <div>Партнёры</div>
            </div>
            <div class="block-partners">
                <div class="partn partn-1">
                    <div class="item ya">
                        <div class="logo"></div>
                        <div class="description">
                            <span>Сайт онлайн-аниме</span>
                            <span>yummyanime.com</span>
                        </div>
                    </div>
                    <div class="item sr">
                        <div class="logo"></div>
                        <div class="description">
                            <span>Сайт онлайн-аниме</span>
                            <span>sovetromantica.com</span>
                        </div>
                    </div>
                    <div class="item jb">
                        <div class="logo"></div>
                        <div class="description">
                            <span>Онлайн-магазин японских вкусняшек</span>
                            <span>j-by.ru</span>
                        </div>
                    </div>
                </div>
                <div class="partn partn-2">
                    <div class="item ad">
                        <div class="logo"></div>
                        <div class="description">
                            <span>Перевод и озвучка аниме</span>
                            <span>anime.anidub.com</span>
                        </div>
                    </div>
<!--                    <div class="item za">-->
<!--                        <div class="logo"></div>-->
<!--                        <div class="description">-->
<!--                            <span>Организация мероприятий</span>-->
<!--                            <span>zilarena.ru</span>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="item amino">
                        <div class="logo"></div>
                        <div class="description">
                            <span>Аниме-сообщество в<br>приложении Амино</span>
                            <span>aminoapps.com/c/russkii-anime</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block journalists" id="journalists">
        <div class="width">
            <div class="title-block">
                <div>Журналисты</div>
            </div>
            <div class="block-journ">
                <h2>Онлайн-трансляции на YouTube!</h2>
                <div class="text-block">
                    <div class="journ-1">
                        <p class="explain">Все происходящее будут снимать приглашенные журналисты, вы сможете посмотреть онлайн-трансляции на Youtube и&nbsp;во&nbsp;всех крупнейших аниме-пабликах в VK.</p>
                        <span>Следите за новостями, мы вышлем напоминание о&nbsp;трансляции на почту:</span>
                        <div class="form" id="remind">
                            <div class="block-input">
                                <input type="text" placeholder="Введите ваш e-mail">
                                <i class="mdi mdi-alert-circle" data-pos="left"></i>
                            </div>
                            <div class="button">Отправить</div>
                        </div>
                    </div>
                    <div class="journ-2">
                        <div class="item">
                            <figure>
                                <img src="/images/kinijin.png" alt="kinijin">
                            </figure>
                            <div class="descriptions">
                                <h3>Kinnijin</h3>
                                <div class="subscribers">
                                    <i class="mdi mdi-youtube-play"></i>
                                    <span>более 65 000</span>
                                </div>
                                <span>подписчиков</span>
                            </div>
                        </div>
                        <div class="item">
                            <figure>
                                <img src="/images/xd_club.jpg" alt="xd_club">
                            </figure>
                            <div class="descriptions">
                                <h3>XD Club</h3>
                                <div class="subscribers">
                                    <i class="mdi mdi-youtube-play"></i>
                                    <span>более 5 000</span>
                                </div>
                                <span>подписчиков</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<footer id="footer">
    <div class="width">
        <div class="info">
            <div class="item contacts">
                <div class="title">Контакты</div>
                <div class="cont">
                    <a href="tel:<?php echo Yii::$app->params['phone']; ?>"><?php echo Yii::$app->params['phone']; ?></a>
                    <a href="mailto:<?php echo Yii::$app->params['projectEmail']; ?>"><?php echo Yii::$app->params['projectEmail']; ?></a>
                    <a target="_blank" href="https://vk.com/vaturn">Группа в ВК</a>
                </div>
            </div>
            <div class="item doc">
                <div class="title">Документы</div>
                <div class="cont">
                    <a target="_blank" href="/doc/oferta.pdf">Публичная оферта</a>
                    <a target="_blank" href="/doc/oplata.pdf">Оплата онлайн</a>
                    <a target="_blank" href="/doc/vozvrat.pdf">Гарантия возврата</a>
                </div>
            </div>
            <div class="item supp">
                <div class="title">Связвться с нами</div>
                <div class="cont">
                    <div class="form" id="support">
                        <div class="block-input">
                            <i class="mdi mdi-alert-circle" data-pos="right"></i>
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="block-input">
                            <i class="mdi mdi-alert-circle" data-pos="right"></i>
                            <textarea placeholder="Ваш вопрос:"></textarea>
                        </div>
                        <div class="button">Отправить</div>
                    </div>
                    <p class="text-success">Ваше сообщение успешно отправлено!</p>
                </div>
            </div>
            <div class="age-limit">
              <div class="age">14+</div>
              <span>Мероприятие рассчитано на возраст от 14 лет, посещение лицами<br>младшего возраста возможно только в сопровождениии взрослых.</span>
            </div>
        </div>
    </div>
    <div class="sub">
        <div class="width">
            <div>© 2018 ИП Олтаржевский Д. О. Все права защищены.</div>
            <div>
                Developer: <a target="_blank" href="https://vk.com/thishibiki">Dmitrieva Galina</a><br>
                Web-designer: <a target="_blank" href="https://vk.com/dazhdia">Shishkina Dazhdia</a>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript">
//    $('#tickets').selectmenu();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDf-hsTgqnZkyUEnOtvyinarywEN1hDLMc&amp;callback=initMap"></script>
